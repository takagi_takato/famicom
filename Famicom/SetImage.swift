//
//  image.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/16.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import Foundation

class SetImage{
    func icon(iconNumber:Int) -> String{
        var iconimg:String = String()
        
        switch iconNumber{
        case 0:
            iconimg = "hito"
            break
        case 1:
            iconimg = "chicken"
            break
            
        case 2:
            iconimg = "cat"
            break
            
        case 3:
            iconimg = "dog"
            break
            
        case 4:
            iconimg = "pig"
            break
        case 5:
            iconimg = "turtle"
            break
        case 6:
            iconimg = "rabbit"
            break
        case 7:
            iconimg = "goat"
            break
        case 8:
            iconimg = "cow"
            break

            
        default:
            break
        }
        return iconimg
    }

}