//
//  UserInfo.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/29.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    
    let config = NSUserDefaults.standardUserDefaults()
    
    func newUserInfo(groupId:String,username:String,imageNum:String){
        
        config.objectForKey("groupId")
        config.objectForKey("username")
        config.objectForKey("imageNum")
        
        config.setObject(groupId,forKey:"groupId")
        config.setObject(username,forKey:"username")
        config.setObject(imageNum,forKey:"imageNum")
        
        
        ncmbClass.loginGroup(groupId,name:username,imageNum:imageNum)
    }
    
}
