//
//  UserCell.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/09.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var username: UILabel!
    var info:BBSInfo = BBSInfo()
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        username.layer.borderWidth = 1
        username.layer.cornerRadius = 8
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
