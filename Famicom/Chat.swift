//
//  Chat.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/24.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//
protocol ChatViewControllerDelegate {
    func modalDidFinished(text: String)
}

import UIKit

class Chat: UIViewController, UITableViewDelegate, UITableViewDataSource, UIToolbarDelegate{
    var txtActiveField = UITextField()
    
    var delegate: ChatViewControllerDelegate! = nil
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    var infos:Array<BBSInfo> = []
    
    let config = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chatMessageForm: UITextField!
    @IBOutlet weak var toolbar: UIToolbar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.delegate = self
        
        chatTable!.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        chatTable.dataSource = self
        chatTable.delegate = self
        
        chatTable.estimatedRowHeight = 90
        
        chatTable.rowHeight = UITableViewAutomaticDimension
        
        chatTable.separatorColor = UIColor.clearColor()
        
        
        // ユーザ情報の取得
        var configId: AnyObject? = config.objectForKey("groupId")
        
        infos = ncmbClass.chatDate(configId as! String)
        
        
        //１秒毎に取得
        var timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "show", userInfo: nil, repeats: true)

        
    }
    
    //NSTimer
    
    internal func show() {
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("groupId")
        infos = ncmbClass.chatDate(configId as! String)
        
        self.chatTable.reloadData()
    }
    
    //toolbarDelegate
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: "handleKeyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: "handleKeyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
//    override func viewDidAppear(animated: Bool) {
//        self.chatTable.setContentOffset(CGPointMake(0, self.chatTable.contentSize.height - self.chatTable.frame.size.height), animated: false)
//        
//    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        return nil;
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infos.count
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var objectId: AnyObject? = config.objectForKey("objectId")
        
        var nib  = UINib(nibName: "ChatCell", bundle:nil)
        chatTable.registerNib(nib, forCellReuseIdentifier:"Cell")
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as?ChatCell
        
        
        if(objectId as! String == infos[indexPath.row].userId){
            
            cell!.chatmessage.layer.borderColor = UIColor.greenColor().CGColor
            
        }
        
        var img:SetImage = SetImage()
        
        
        cell!.chatmessage.text = infos[indexPath.row].message
        cell!.username.text = infos[indexPath.row].name
        cell!.chatDate.text = infos[indexPath.row].date
        cell!.chatTime.text = infos[indexPath.row].time
        
        var image = UIImage(named:img.icon(infos[indexPath.row].iconNumber.toInt()!))
        
        let size = CGSize(width: 40, height: 40)
        UIGraphicsBeginImageContext(size)
        image!.drawInRect(CGRectMake(0, 0, size.width, size.height))
        var resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        cell!.userIcon.image = resizeImage
        
        
        cell!.chatmessage.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        return cell!
        
        
        
    }
    
    @IBAction func submitTap(sender: AnyObject) {
        
        if(count(chatMessageForm.text) == 0){
            let alertController = UIAlertController(title: "メッセージを入力してください。", message: "", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }else{
            
            //日付取得
            
            //現在時刻を取得.
            let myDate: NSDate = NSDate()
            
            //カレンダーを取得.
            let myCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            
            let myComponetns = myCalendar.components(NSCalendarUnit.CalendarUnitYear   |
                NSCalendarUnit.CalendarUnitMonth  |
                NSCalendarUnit.CalendarUnitDay    |
                NSCalendarUnit.CalendarUnitHour   |
                NSCalendarUnit.CalendarUnitMinute |
                NSCalendarUnit.CalendarUnitSecond |
                NSCalendarUnit.CalendarUnitWeekday,
                fromDate: myDate)
            
            let weekdayStrings: Array = ["nil","日","月","火","水","木","金","土","日"]
            
            var myStr: String = "\(myComponetns.month)/"
            myStr += "\(myComponetns.day)"
            
            
            
            var time:String = "\(myComponetns.hour):"
            time += "\(myComponetns.minute)"
            
            
            let config = NSUserDefaults.standardUserDefaults()
            var configId: AnyObject? = config.objectForKey("groupId")
            
            var objectId: AnyObject? = config.objectForKey("objectId")
            
            var username: AnyObject? = config.objectForKey("username")
            
            var image: AnyObject? = config.objectForKey("imageNum")
            
            ncmbClass.saveChatMessage(self.chatMessageForm.text, name: username as! String, date: myStr, groupId: configId as! String,userId:objectId as! String,imageNum:image as! String,time:time)
            
            infos = ncmbClass.chatDate(configId as! String)
            
            self.chatTable.reloadData()
            
            self.chatTable.setContentOffset(CGPointMake(0, self.chatTable.contentSize.height - self.chatTable.frame.size.height), animated: false)
            
            self.view.endEditing(true)

        }
    }
    
    //scroll view
    func textFieldShouldBeginEditing(textField: UITextField!) -> Bool {
        txtActiveField = textField
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func handleKeyboardWillShowNotification(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        let txtLimit = txtActiveField.frame.origin.y + txtActiveField.frame.height + 8.0
        let kbdLimit = myBoundSize.height - keyboardScreenEndFrame.size.height
        if txtLimit >= kbdLimit {
            scrollView.contentOffset.y = txtLimit - kbdLimit + 70
        }
    }
    
    func handleKeyboardWillHideNotification(notification: NSNotification) {
        scrollView.contentOffset.y = 0
    }
    
    
    
    @IBAction func tapScreen(sender: AnyObject) {
        self.view.endEditing(true)
    }
    @IBAction func backPage(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func modalDidFinished(tag: String) {
    }
}
