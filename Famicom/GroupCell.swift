//
//  GroupCell.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/16.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
