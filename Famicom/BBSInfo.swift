//
//  BBSInfo.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/15.
//  Copyright (c) 2015年 infonear inc. All rights reserved.
//

import Foundation

struct BBSInfo {
    /* ユーザ情報 */
    var userId:String = String()
    var name:String = String()


    /* 日記情報 */
    var objectId:String = String()
    var message:String = String()
    var date:String = String()
    var iconNumber:String = String()
    
    /*CHAT*/
    var chatDate:String = String()
    var time:String = String()

}

