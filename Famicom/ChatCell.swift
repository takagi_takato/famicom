//
//  ChatCell.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/24.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var chatDate: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var chatmessage: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var chatTime: UILabel!
    
    var setday:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization cod
        
        chatmessage.layer.cornerRadius = 15
        chatmessage.clipsToBounds = true
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
