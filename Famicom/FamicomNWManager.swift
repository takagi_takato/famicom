//
//  NCMB.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/08.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import Foundation

class FamicomNWManager{
    
    
    
    func getUser(groupId:String) -> Array<BBSInfo> {
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        var infos: Array<BBSInfo> =  Array<BBSInfo>()
        
        var query: NCMBQuery = NCMBQuery(className: "USER")
        
        query.whereKey("groupId", equalTo: "\(groupId)")
        
        let objects:NSArray = query.findObjects(nil)
        for(var i = 0;i < objects.count;i++){
            var info:BBSInfo = BBSInfo()

            info.userId = objects[i].objectForKey("objectId") as! String
            info.iconNumber = objects[i].objectForKey("image") as! String
            info.name = objects[i].objectForKey("name") as! String
            
            infos.append(info)
        }
        return infos
    }

    
     func saveMessage(message:String, info:BBSInfo){
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        var query: NCMBQuery = NCMBQuery(className: "BBS")
        let objects:NSArray = query.findObjects(nil)
                    var saveError : NSError? = nil
                    var obj : NCMBObject = NCMBObject(className: "BBS")
        if(info.objectId.isEmpty){
            
            obj.setObject(info.userId, forKey: "userId")
            obj.setObject(info.date, forKey: "date")

        }else{
            obj.objectId = info.objectId
        }
        obj.setObject("\(message)", forKey: "message")
        obj.save(&saveError)
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")

        }
        
    }
    
    
    func getMessage(userId:String,date:String) -> BBSInfo{
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        
        
        var query: NCMBQuery = NCMBQuery(className: "BBS")
        var info:BBSInfo = BBSInfo()

        query.whereKey("userId", equalTo: "\(userId)")
        query.whereKey("date", equalTo: "\(date)")

        let objects:NSArray = query.findObjects(nil)
        if(objects.count > 0)
            {
            info.message = objects[0].objectForKey("message") as! String
            info.objectId = objects[0].objectForKey("objectId") as! String
                
                info.userId = objects[0].objectForKey("userId") as! String
                info.date = objects[0].objectForKey("date") as! String
            }
        
        return info
    }
    func deleteUser(userId:String){
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        var query: NCMBQuery = NCMBQuery(className: "USER")
        let objects:NSArray = query.findObjects(nil)
        var saveError : NSError? = nil
        var obj : NCMBObject = NCMBObject(className: "USER")

        obj.objectId = userId as String
        
        obj.delete(&saveError)
    
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")
            
        }
    }
    func seveUserData(userId:String,name:String,imageNum:String){
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        var query: NCMBQuery = NCMBQuery(className: "USER")
        let objects:NSArray = query.findObjects(nil)
        var saveError : NSError? = nil
        var obj : NCMBObject = NCMBObject(className: "USER")
    
        
        obj.objectId = userId
        
        obj.setObject("\(name)", forKey: "name")
        obj.setObject("\(imageNum)", forKey: "image")

        obj.save(&saveError)
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")
            
        }
        
    }
    func loginGroup(groupId:String,name:String,imageNum:String){
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        
        //USERクラスに格納
        var userQuery: NCMBQuery = NCMBQuery(className: "USER")
        let objects:NSArray = userQuery.findObjects(nil)
        var saveError : NSError? = nil
        var obj : NCMBObject = NCMBObject(className: "USER")
        
        
        obj.setObject("\(groupId)", forKey: "groupId")
        obj.setObject("\(name)", forKey: "name")
        obj.setObject("\(imageNum)", forKey: "image")

        obj.save(&saveError)
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")
            
        }

        //USERのobjectId取得
        userQuery.whereKey("name", equalTo: "\(name)")
        userQuery.whereKey("groupId", equalTo: "\(groupId)")
        userQuery.whereKey("image", equalTo: "\(imageNum)")
        
        
        let objects3:NSArray = userQuery.findObjects(nil)
        
        var conf = objects3[objects3.count - 1].objectForKey("objectId") as! String
        println(conf)
        
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("objectId")
        config.setObject(conf,forKey:"objectId")
        
        
        //GROUPクラスに格納
        var groupQuery: NCMBQuery = NCMBQuery(className: "GROUP")
        let objects2:NSArray = groupQuery.findObjects(nil)
        var saveError2 : NSError? = nil
        var obj2 : NCMBObject = NCMBObject(className: "GROUP")
        
        if(objects2.count > 0){
        
        obj2.setObject("\(groupId)", forKey: "groupId")

        obj2.save(&saveError2)
        }
        
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")
            
        }
    }
    
    func confirmUser(objectId:String) -> String{
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        
        var query: NCMBQuery = NCMBQuery(className: "USER")
        var info:String!
        
        query.whereKey("objectId", equalTo: "\(objectId)")
        
        let objects:NSArray = query.findObjects(nil)
        if(objects.count > 0){
            info = objects[0].objectForKey("name") as! String
        }else{
            info = "nil"
        }
        println(info)
        return info
    }
    
    func chatDate(groupId:String)  -> Array<BBSInfo>{
        
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        
        var infos: Array<BBSInfo> =  Array<BBSInfo>()
        
        var query: NCMBQuery = NCMBQuery(className: "CHAT")
        
        query.whereKey("groupId", equalTo: "\(groupId)")
        
        let objects:NSArray = query.findObjects(nil)
        for(var i = 0;i < objects.count;i++){
            var info:BBSInfo = BBSInfo()
            
            info.userId = objects[i].objectForKey("objectId") as! String
            info.iconNumber = objects[i].objectForKey("image") as! String
            info.name = objects[i].objectForKey("name") as! String
            info.message = objects[i].objectForKey("message") as! String
            info.message = objects[i].objectForKey("message") as! String
            info.userId = objects[i].objectForKey("userId") as! String
            info.date = objects[i].objectForKey("date") as! String
            info.time = objects[i].objectForKey("time") as! String
            
            infos.append(info)
        }
        return infos
    }
    func saveChatMessage(message:String, name:String, date:String, groupId:String, userId:String, imageNum:String, time:String){
        NCMB.setApplicationKey("a7dc73aafd8e43e853aa923c0c12bf7ff34ccf40c88f8564b3ca32d3d3bc59fc",
            clientKey: "a2aab38166628371ce85abb582c099feee4eb36fbdd00e2789be3e763d5f4463")
        var query: NCMBQuery = NCMBQuery(className: "CHAT")
        let objects:NSArray = query.findObjects(nil)
        var saveError : NSError? = nil
        var obj : NCMBObject = NCMBObject(className: "CHAT")
        
        obj.setObject("\(name)", forKey: "name")
        obj.setObject("\(date)", forKey: "date")
        obj.setObject("\(message)", forKey: "message")
        obj.setObject("\(groupId)", forKey: "groupId")
        obj.setObject("\(imageNum)", forKey: "image")
        obj.setObject("\(userId)", forKey: "userId")
        obj.setObject("\(time)", forKey: "time")
        
        
        obj.save(&saveError)
        
        if(saveError == nil){
            println("[SAVE] Done.")
        } else {
            println("[SAVE ERROR] \(saveError)")
            
        }
        
    }
}
