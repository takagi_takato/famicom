//
//  MessageWindow.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/10.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

protocol ModalViewControllerDelegate {
    func modalDidFinished(text: String)
}

class MessageWindow: UIViewController {
    
    
    var delegate: ModalViewControllerDelegate! = nil
    var info:BBSInfo = BBSInfo()
    
    @IBOutlet weak var messageview: UIView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var messageDay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var img:SetImage = SetImage()
        
        messageview.layer.borderWidth = 1
        message.text = info.message
        messageDay.text = info.date
        
        icon.image = UIImage(named:img.icon(info.iconNumber.toInt()!))
        
        
        var myKeyboard = UIView(frame: CGRectMake(0, 0, 320, 40))
        myKeyboard.backgroundColor = UIColor.lightGrayColor()
        
        // Doneボタン作成
        var myButton = UIButton(frame: CGRectMake(80, 5, 200, 30))
        myButton.backgroundColor = UIColor.lightGrayColor()
        myButton.setTitle("保存", forState: UIControlState.Normal)
        myButton.layer.borderWidth = 1
        myButton.layer.cornerRadius = 10
        myButton.addTarget(self, action: "saveButton", forControlEvents: UIControlEvents.TouchUpInside)
        
        // ボタンをビューに追加
        myKeyboard.addSubview(myButton)
        
        // ビューをフィールドに設定
        message.inputAccessoryView = myKeyboard
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCloseed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func saveButton () {
        
        var ncmbClass:FamicomNWManager = FamicomNWManager()
        
        ncmbClass.saveMessage("\(message.text)", info:self.info)
        
        self.delegate.modalDidFinished(message.text)
        self.view.endEditing(true )
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
}