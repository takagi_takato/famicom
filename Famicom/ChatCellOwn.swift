//
//  ChatCellOwn.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/24.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class ChatCellOwn: UITableViewCell {

    @IBOutlet weak var chatMessage: UILabel!
    @IBOutlet weak var username: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        chatMessage.layer.cornerRadius = 10
        chatMessage.clipsToBounds = true


    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
