//
//  FirstView.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/15.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit
protocol FirstViewControllerDelegate {
    func modalDidFinished(text: String)
}


class FirstView: UIViewController, UIToolbarDelegate,IconViewControllerDelegate{
    
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    
    var delegate: FirstViewControllerDelegate! = nil
    var myInfo:UserInfo = UserInfo()
    
    @IBOutlet weak var groupIdForm: UITextField!
    @IBOutlet weak var nameForm: UITextField!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var enterBtn: UIButton!
    @IBOutlet weak var toolbar: UIToolbar!
    
    var imageURL:String!
    var imageNum:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.delegate = self
        
        if (imageURL == nil){
            imageURL = "hito"
        }
        iconButton.setImage(UIImage(named:imageURL), forState: .Normal)
        iconButton.layer.borderWidth = 1
        
        
        
        
    }
    
    //toolbarDelegate
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
        
        
        if (imageNum == nil){
            imageNum = "0"
        }
        
        if(count(groupIdForm.text) == 0)||(count(nameForm.text) == 0){
            let alertController = UIAlertController(title: "フォームが未入力です。", message: "", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
        }else{
            
            myInfo.newUserInfo(groupIdForm.text,username:nameForm.text,imageNum:imageNum)
      
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let next: UIViewController = storyboard.instantiateInitialViewController() as! UIViewController
            next.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            presentViewController(next, animated: true, completion: nil)
            
        }
        
    }
    @IBAction func iconSelect(sender: AnyObject) {
        
        let modalView = IconSelect(nibName: "IconSelect", bundle: nil)
        modalView.delegate = self
        modalView.beforeView = "First"
        modalView.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        
        self.presentViewController(modalView, animated: true, completion: nil)
    }
    
    func modalDidFinished(tag: String) {
        var img:SetImage = SetImage()
        imageNum = tag
        imageURL = img.icon(tag.toInt()!)
        
        iconButton.setImage(UIImage(named:imageURL), forState: .Normal)
        
    }
    
}
