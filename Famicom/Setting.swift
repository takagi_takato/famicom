//
//  Setting.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/16.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit
protocol BBSViewControllerDelegate{
    func modalDidFinished2(text: String)
}

class Setting: UIViewController,UITableViewDelegate, UITableViewDataSource, UIToolbarDelegate, UserViewControllerDelegate{
    
    var delegate: BBSViewControllerDelegate! = nil
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    var bbs:BBSCalendar = BBSCalendar()
    var info:BBSInfo = BBSInfo()
    var infos:Array<BBSInfo> = []
    var mySections: NSArray = ["家族情報", ""]
    
    @IBOutlet weak var groupId: UILabel!
    @IBOutlet weak var setTable: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.delegate = self
        
        // Cell名の登録をおこなう.
        setTable!.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        // DataSourceの設定をする.
        setTable.dataSource = self
        
        // Delegateを設定する.
        setTable.delegate = self
        
        var nib  = UINib(nibName: "GroupCell", bundle:nil)
        setTable.registerNib(nib, forCellReuseIdentifier:"Cell")
        
        //ユーザー情報取得
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("groupId")
        var configIdstr = configId as! String
        groupId.text = ("GROUP ID  :  [ \(configIdstr) ]")
        infos = ncmbClass.getUser(configIdstr)
        
        
    }
    
    //toolbarDelegate
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section] as? String
    }
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let modalView = UserWindow(nibName: "UserWindow", bundle: nil)
        modalView.delegate = self
        
        modalView.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        modalView.info.userId = infos[indexPath.row].userId
        modalView.info.name = infos[indexPath.row].name
        modalView.info.iconNumber = infos[indexPath.row].iconNumber
        
        self.presentViewController(modalView, animated: true, completion: nil)
        
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return infos.count
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var img:SetImage = SetImage()
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as?GroupCell
        
        cell?.username.text = infos[indexPath.row].name
        
        cell?.userIcon.image = UIImage(named:img.icon(infos[indexPath.row].iconNumber.toInt()!))
        
        return cell!
        
    }
    func modalDidFinished(text:String) {
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("groupId")
        var configIdstr = configId as! String
        infos = ncmbClass.getUser(configIdstr)
        
        self.setTable.reloadData()
    }
    
    @IBAction func backPage(sender: AnyObject) {
        self.delegate.modalDidFinished2("aaaa")
        self.view.endEditing(true )
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}
