//
//  ViewController.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/01.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class BBSCalendar: UIViewController, UITableViewDelegate, UITableViewDataSource, UIToolbarDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate, ModalViewControllerDelegate, BBSViewControllerDelegate, ChatViewControllerDelegate{
    
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    var infos:Array<BBSInfo> = []
    
    @IBOutlet weak var toolbar: UIToolbar!
    //メンバ変数の設定（配列格納用）
    var mArray: NSMutableArray! = NSMutableArray()
    //メンバ変数の設定（カレンダー用）
    var year: Int!
    var month: Int!
    var day: Int!
    var maxDay: Int!
    var dayOfWeek: Int!
    
    //メンバ変数の設定（カレンダー関数から取得したものを渡す）
    var comps: NSDateComponents!
    
    
    //プロパティを指定
    @IBOutlet weak var calendarBar: UILabel!
    @IBOutlet weak var dayBar: UILabel!
    @IBOutlet var FamilyTableView: UITableView!
    @IBOutlet weak var prevMonthButton: UIBarButtonItem!
    @IBOutlet weak var nextMonthButton: UIBarButtonItem!
    @IBOutlet weak var DayCollectionView: UICollectionView!

    
    var calendarLabelIntervalX: Int!
    var calendarLabelX: Int!
    var calendarLabelY: Int!
    var calendarLabelWidth: Int!
    var calendarLabelHeight: Int!
    var calendarLableFontSize: Int!
    //modal
    
    @IBOutlet weak var settingBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toolbar.delegate = self
        self.dayBar.layer.borderWidth = 1
        self.dayBar.layer.borderColor = UIColor.lightGrayColor().CGColor
//        self.dayBar.layer.cornerRadius = 15
//        self.dayBar.clipsToBounds = true
        
        var now: NSDate!
        
        settingBtn.setImage(UIImage(named:"ie"), forState: .Normal)
        chatBtn.setImage(UIImage(named:"chat"), forState: .Normal)
        
        
        // Cell名の登録をおこなう.
        FamilyTableView!.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        // DataSourceの設定をする.
        FamilyTableView.dataSource = self
        
        // Delegateを設定する.
        FamilyTableView.delegate = self
        
        var nib  = UINib(nibName: "UserCell", bundle:nil)
        FamilyTableView.registerNib(nib, forCellReuseIdentifier:"Cell")
        
        
        
        // ユーザ情報の取得
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("groupId")
        
        infos = ncmbClass.getUser(configId as! String)
        
        
        //現在起動中のデバイスを取得（スクリーンの幅・高さ）
        let screenWidth  = DeviseSize.screenWidth()
        let screenHeight = DeviseSize.screenHeight()
        
        //iPhone4s
        if(screenWidth == 320 && screenHeight == 480){
            
            
            //iPhone5またはiPhone5s
        }else if (screenWidth == 320 && screenHeight == 568){
            
            calendarLabelIntervalX = 5;
            calendarLabelX         = 45;
            calendarLabelY         = 30;
            calendarLabelWidth     = 40;
            calendarLabelHeight    = 100;
            calendarLableFontSize  = 14;
            
            //iPhone6
        }else if (screenWidth == 375 && screenHeight == 667){
        
            calendarLabelIntervalX = 7;
            calendarLabelX         = 52;
            calendarLabelY         = 30;
            calendarLabelWidth     = 42;
            calendarLabelHeight    = 100;
            calendarLableFontSize  = 14;
            
            
            //iPhone6 plus
        }else if (screenWidth == 414 && screenHeight == 736){
            
            
        }
        
        
        //現在の日付を取得する
        now = NSDate()
        
        //inUnit:で指定した単位（月）の中で、rangeOfUnit:で指定した単位（日）が取り得る範囲
        var calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var range: NSRange = calendar.rangeOfUnit(NSCalendarUnit.CalendarUnitDay, inUnit:NSCalendarUnit.CalendarUnitMonth, forDate:now)
        
        //最初にメンバ変数に格納するための現在日付の情報を取得する
        comps = calendar.components(NSCalendarUnit.CalendarUnitYear|NSCalendarUnit.CalendarUnitMonth|NSCalendarUnit.CalendarUnitDay|NSCalendarUnit.CalendarUnitWeekday,fromDate:now)
        
        //年月日と最後の日付と曜日を取得(NSIntegerをintへのキャスト不要)
        
        year      = comps.year
        month     = comps.month
        day       = comps.day
        dayOfWeek = comps.weekday
        maxDay    = range.length
        
        //曜日ラベル初期定義
        var monthName:[String] = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
        
        //曜日ラベルを動的に配置
        setupCalendarLabel(monthName)
        
        //初期表示時のカレンダーをセットアップする
        setupCurrentCalendar()
        
    }
    
    //曜日ラベルの動的配置関数
    func setupCalendarLabel(array: NSArray) {
        
        var calendarLabelCount = 7
        
        for i in 0...6{
            
            //ラベルを作成
            var calendarBaseLabel: UILabel = UILabel()
            
            //X座標の値をCGFloat型へ変換して設定
            calendarBaseLabel.frame = CGRectMake(
                CGFloat(calendarLabelIntervalX + calendarLabelX * (i % calendarLabelCount)),
                CGFloat(calendarLabelY),
                CGFloat(calendarLabelWidth),
                CGFloat(calendarLabelHeight)
            )
            
            //日曜日の場合は赤色を指定
            if(i == 0){
                
                //RGBカラーの設定は小数値をCGFloat型にしてあげる
                calendarBaseLabel.textColor = UIColor(
                    red: CGFloat(0.831), green: CGFloat(0.349), blue: CGFloat(0.224), alpha: CGFloat(1.0)
                )
                
                //土曜日の場合は青色を指定
            }else if(i == 6){
                
                //RGBカラーの設定は小数値をCGFloat型にしてあげる
                calendarBaseLabel.textColor = UIColor(
                    red: CGFloat(0.400), green: CGFloat(0.471), blue: CGFloat(0.980), alpha: CGFloat(1.0)
                )
                
                //平日の場合は灰色を指定
            }else{
                
                //既に用意されている配色パターンの場合
                calendarBaseLabel.textColor = UIColor.lightGrayColor()
                
            }
            
            //曜日ラベルの配置
            calendarBaseLabel.text = String(array[i] as! NSString)
            calendarBaseLabel.textAlignment = NSTextAlignment.Center
            calendarBaseLabel.font = UIFont(name: "System", size: CGFloat(calendarLableFontSize))
            self.view.addSubview(calendarBaseLabel)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //toolbarDelegate
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    

    
    //タイトル表記を設定する関数
    func setupCalendarTitleLabel() {
        calendarBar.text = String("\(year)年\(month)月")
    }
    
    func setupDayBarTitleLabel(){
        let date = NSDate()      // 現在日時
        let calendar = NSCalendar.currentCalendar()
        var comp : NSDateComponents = calendar.components(
            NSCalendarUnit.CalendarUnitDay, fromDate: date)
        dayBar.text = String("\(year)年\(month)月\(comp.day)日")
    }
    
    //現在（初期表示時）の年月に該当するデータを取得する関数
    func setupCurrentCalendarData() {
        
        var currentCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var currentComps: NSDateComponents = NSDateComponents()
        
        currentComps.year  = year
        currentComps.month = month
        currentComps.day   = 1
        
        var currentDate: NSDate = currentCalendar.dateFromComponents(currentComps)!
    }
    
    //前の年月に該当するデータを取得する関数
    func setupPrevCalendarData() {
        
        //現在の月に対して-1をする
        if(month == 0){
            year = year - 1;
            month = 12;
        }else{
            month = month - 1;
        }
        
        //setupCurrentCalendarData()と同様の処理を行う
        var prevCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var prevComps: NSDateComponents = NSDateComponents()
        
        prevComps.year  = year
        prevComps.month = month
        prevComps.day   = 1
        
        var prevDate: NSDate = prevCalendar.dateFromComponents(prevComps)!
        recreateCalendarParameter(prevCalendar, currentDate: prevDate)
    }
    
    //次の年月に該当するデータを取得する関数
    func setupNextCalendarData() {
        
        //現在の月に対して+1をする
        if(month == 12){
            year = year + 1;
            month = 1;
        }else{
            month = month + 1;
        }
        
        //setupCurrentCalendarData()と同様の処理を行う
        var nextCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        var nextComps: NSDateComponents = NSDateComponents()
        
        nextComps.year  = year
        nextComps.month = month
        nextComps.day   = 1
        
        var nextDate: NSDate = nextCalendar.dateFromComponents(nextComps)!
       recreateCalendarParameter(nextCalendar, currentDate: nextDate)
    }
    
    //カレンダーのパラメータを再作成する関数
    func recreateCalendarParameter(currentCalendar: NSCalendar, currentDate: NSDate) {
        
        //引数で渡されたものをもとに日付の情報を取得する
        var currentRange: NSRange = currentCalendar.rangeOfUnit(NSCalendarUnit.CalendarUnitDay, inUnit:NSCalendarUnit.CalendarUnitMonth, forDate:currentDate)
        
        comps = currentCalendar.components(NSCalendarUnit.CalendarUnitYear|NSCalendarUnit.CalendarUnitMonth|NSCalendarUnit.CalendarUnitDay|NSCalendarUnit.CalendarUnitWeekday,fromDate:currentDate)
        
        //年月日と最後の日付と曜日を取得(NSIntegerをintへのキャスト不要)
        year      = comps.year
        month     = comps.month
        day       = comps.day
        dayOfWeek = comps.weekday - 2
        maxDay    = currentRange.length
    }
    
    
    //現在のカレンダーをセットアップする関数
    func setupCurrentCalendar() {
        
        setupCurrentCalendarData()
        setupCalendarTitleLabel()
        setupDayBarTitleLabel()
        
    }
    
    //前の月のボタン,swipeのアクション
    @IBAction func getPrevMonthData(sender: AnyObject) {
        setupPrevCalendarData()
        setupCalendarTitleLabel()
        DayCollectionView.reloadData()
    }
    
    //次の月のボタンを押した際のアクション
    @IBAction func getNextMonthData(sender: AnyObject) {
        setupNextCalendarData()
        setupCalendarTitleLabel()
        DayCollectionView.reloadData()

    }
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let modalView = MessageWindow(nibName: "MessageWindow", bundle: nil)
        modalView.delegate = self
        
        
        var infoSet:Setting = Setting()
        
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)  as! UserCell
        
        println("userId: \(infos[indexPath.row].userId)")
        println("iconNum: \(infos[indexPath.row].iconNumber)")
        println("name: \(infos[indexPath.row].name)")
        
        
        modalView.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        
        modalView.info = cell.info
        
        self.presentViewController(modalView, animated: true, completion: nil)
        
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infos.count
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var img:SetImage = SetImage()
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as?UserCell
        
        var selectDay = dayBar.text!
        var userId: String = infos[indexPath.row].userId as String
        var name: String = infos[indexPath.row].name as String
        
        var info:BBSInfo = ncmbClass.getMessage("\(userId)",date:"\(selectDay)")
        
        //メッセージ無くてもuser情報はつけておく
        if info.objectId.isEmpty {
            info.userId = infos[indexPath.row].userId as String
            info.name = infos[indexPath.row].name as String
            info.date = dayBar.text!
            
        }
        
        info.iconNumber = infos[indexPath.row].iconNumber
        cell!.username.text = infos[indexPath.row].name as String
        cell!.message.text = info.message
        cell!.info = info
        cell!.icon.image = UIImage(named:img.icon(info.iconNumber.toInt()!))
        
        return cell!
        
    }
    
    //MARK: collectionview
    /*
    Cellに値を設定する
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell:DayCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DayCell
        println("Num: \(indexPath.row)")
        
        var num = indexPath.row + 1
        var dayNum = indexPath.row - dayOfWeek
        println(dayOfWeek)
        
        if(dayOfWeek >= indexPath.row){
            
            cell.day.text = ""
            
        }else if(maxDay < dayNum){
            cell.day.text = ""
        }else{
            
            cell.day.text = String(dayNum)
        }
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        return cell
    }
    
    /*
    Cellが選択された際に呼び出される
    */
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var dayNum = indexPath.row - dayOfWeek

        let cell:DayCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DayCell
        
        println("Num: \(dayNum)")
        dayBar.text = String("\(year)年\(month)月\(dayNum)日")
        
        self.FamilyTableView.reloadData()
    }
    
    /*
    Cellの総数を返す
    */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 42
    }
    
    /*
    Cellのサイズを返す
    */
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.size.width/7, self.view.frame.size.width/7 - 8)
    }
    

    
    func modalDidFinished(text: String) {
        self.FamilyTableView.reloadData()
    }
    
    @IBAction func test(sender: AnyObject) {
        let Set = Setting(nibName: "Setting", bundle: nil)
        Set.delegate = self
        
        
        Set.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        
        self.presentViewController(Set, animated: true, completion: nil)
        
    }
    func modalDidFinished2(text: String) {
        // ユーザ情報の再取得
        let config = NSUserDefaults.standardUserDefaults()
        var configId: AnyObject? = config.objectForKey("groupId")
        var configIdstr = configId as! String
        
        infos = ncmbClass.getUser(configIdstr)
        self.FamilyTableView.reloadData()
        
    }
    @IBAction func chatModal(sender: AnyObject) {
        let modalView = Chat(nibName: "Chat", bundle: nil)
        modalView.delegate = self
        
        modalView.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        self.presentViewController(modalView, animated: true, completion: nil)
        
    }
}