//
//  UserWindow.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/16.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

protocol UserViewControllerDelegate{
    func modalDidFinished(text: String)
}

class UserWindow: UIViewController, editIconControllerDelegate, FirstViewControllerDelegate {
    
    var ncmbClass:FamicomNWManager = FamicomNWManager()
    var delegate: UserViewControllerDelegate! = nil
    
    var img:SetImage = SetImage()
    var info:BBSInfo = BBSInfo()
    
    @IBOutlet weak var userWindow: UIView!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var userIcon: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var batu: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameField.text = info.name
        
        saveBtn.layer.cornerRadius = 16
        
        deleteBtn.layer.cornerRadius = 16
        
        
        userIcon.setImage(UIImage(named:img.icon(info.iconNumber.toInt()!)), forState: .Normal)
        
        userWindow.layer.cornerRadius = 20
        
        var myKeyboard = UIView(frame: CGRectMake(0, 0, 320, 40))
        myKeyboard.backgroundColor = UIColor.lightGrayColor()
        
        // Doneボタン作成
        var myButton = UIButton(frame: CGRectMake(80, 5, 200, 30))
        myButton.backgroundColor = UIColor.lightGrayColor()
        myButton.setTitle("保存", forState: UIControlState.Normal)
        myButton.layer.borderWidth = 1
        myButton.layer.cornerRadius = 10
        myButton.addTarget(self, action: "saveButton", forControlEvents: UIControlEvents.TouchUpInside)
        // backButtonボタン作成
        var backButton = UIButton(frame: CGRectMake(5, 5, 50, 30))
        backButton.backgroundColor = UIColor.lightGrayColor()
        backButton.setTitle("戻る", forState: UIControlState.Normal)
        backButton.layer.borderWidth = 1
        backButton.layer.cornerRadius = 10
        backButton.addTarget(self, action: "backButton", forControlEvents: UIControlEvents.TouchUpInside)
        // ボタンをビューに追加
        myKeyboard.addSubview(myButton)
        
        myKeyboard.addSubview(backButton)
        
        // ビューをフィールドに設定
        userNameField.inputAccessoryView = myKeyboard
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func deleteUserData(sender: AnyObject) {
        let alertController = UIAlertController(title: "", message: "ユーザー情報を消してもいいのですか？", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            self.ncmbClass.deleteUser(self.info.userId)
            self.delegate.modalDidFinished("")
            
            var infos:Array<BBSInfo> = []
            
            let config = NSUserDefaults.standardUserDefaults()
            var configId: AnyObject? = config.objectForKey("groupId")
            var configIdstr = configId as! String
            infos = self.ncmbClass.getUser(configIdstr)
            
            if(infos.count == 0){
                if(infos.count == 0){
                    let modalView = FirstView(nibName: "FirstView", bundle: nil)
                    modalView.delegate = self
                    
                    self.presentViewController(modalView, animated: true, completion: nil)
                    
                }
            }else{
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in println("CANCEL")
        }
        
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func saveAction(){
        
        let config = NSUserDefaults.standardUserDefaults()
        var objectId: AnyObject? = config.objectForKey("objectId")
        
        if(objectId as! String == info.userId){
            config.setObject(userNameField.text,forKey:"username")
            config.setObject(info.iconNumber,forKey:"imageNum")
            
        }
        
        self.ncmbClass.seveUserData(self.info.userId,name:userNameField.text,imageNum:info.iconNumber)
        self.view.endEditing(true )
        self.delegate.modalDidFinished("aa")
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    @IBAction func saveTap(sender: AnyObject) {
        saveAction()
    }
    func saveButton () {
        saveAction()
    }
    func backButton () {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func iconTap(sender: AnyObject) {
        
        let modal = IconSelect(nibName: "IconSelect", bundle: nil)
        modal.edit = self
        modal.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        modal.beforeView = "Set"
        
        self.presentViewController(modal, animated: true, completion: nil)
        
    }
    func modalDidFinished(tag: String) {
        var img:SetImage = SetImage()
        
        info.iconNumber = tag
        
        userIcon.setImage(UIImage(named:img.icon(tag.toInt()!)), forState: .Normal)
    }
    
    
    
}
