//
//  DayCell.swift
//  Famicom
//
//  Created by takagi_takato on 2015/07/06.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

class DayCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBOutlet weak var day: UILabel!
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }

}
