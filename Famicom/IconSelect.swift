//
//  IconSelect.swift
//  Famicom
//
//  Created by takagi_takato on 2015/06/15.
//  Copyright (c) 2015年 takagi_takato. All rights reserved.
//

import UIKit

protocol IconViewControllerDelegate {
    func modalDidFinished(text: String)
}
protocol editIconControllerDelegate {
    func modalDidFinished(text: String)
}

class IconSelect: UIViewController {
    
    var img:SetImage = SetImage()
    var delegate: IconViewControllerDelegate! = nil
    var edit: editIconControllerDelegate! = nil
    var beforeView:String!
    
    @IBOutlet weak var iconImage0: UIButton!
    @IBOutlet weak var iconImage1: UIButton!
    @IBOutlet weak var iconImage2: UIButton!
    @IBOutlet weak var iconImage3: UIButton!
    @IBOutlet weak var iconImage4: UIButton!
    @IBOutlet weak var iconImage5: UIButton!
    @IBOutlet weak var iconImage6: UIButton!
    @IBOutlet weak var iconImage7: UIButton!
    @IBOutlet weak var iconImage8: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconImage0.setImage(UIImage(named:img.icon(0)), forState: .Normal)
        iconImage1.setImage(UIImage(named:img.icon(1)), forState: .Normal)
        iconImage2.setImage(UIImage(named:img.icon(2)), forState: .Normal)
        iconImage3.setImage(UIImage(named:img.icon(3)), forState: .Normal)
        iconImage4.setImage(UIImage(named:img.icon(4)), forState: .Normal)
        iconImage5.setImage(UIImage(named:img.icon(5)), forState: .Normal)
        iconImage6.setImage(UIImage(named:img.icon(6)), forState: .Normal)
        iconImage7.setImage(UIImage(named:img.icon(7)), forState: .Normal)
        iconImage8.setImage(UIImage(named:img.icon(8)), forState: .Normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func IconSelect(sender: AnyObject) {
        
        var tag:String = String(sender.tag)
        if(beforeView == "First"){
            self.delegate.modalDidFinished(tag)
        }else if(beforeView == "Set"){
            self.edit.modalDidFinished(tag)        }
        
    }
    
}
